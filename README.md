# Duboko učenje 2

1 - Implementacija modela Gaussove mješavine, probabilističke regresije

2 - Implementacija modela Varijacijskog Autoenkodera

3 - Implementacija modela Boltzmannovih strojeva

4 - Implementacija GAN mreža

5 - Implementacija normalizirajućih tokova
