import math
from typing import Union, Collection

import tensorflow as tf
import numpy as np

TFData= Union[tf.Tensor, tf.Variable, float]


class GMModel:
    def __init__(self, K):
        self.K = K
        self.mean = tf.Variable(tf.random.normal(shape=[K]))
        self.logvar = tf.Variable(tf.random.normal(shape=[K]))
        self.logpi = tf.Variable(tf.zeros(shape=[K]))

    @property
    def variables(self) -> Collection[TFData]:
        return self.mean, self.logvar, self.logpi

    @staticmethod
    def neglog_normal_pdf(x: TFData, mean: TFData, logvar: TFData):
        var = tf.exp(logvar)

        return 0.5 * (tf.math.log(2 * math.pi) + logvar + (x - mean) ** 2 / var)

    @tf.function
    def loss(self, data: TFData):
        lz_const = tf.reduce_logsumexp(self.logpi)
        l = []

        for k in range(self.K):
            lxz = self.neglog_normal_pdf(data, self.mean[k], self.logvar[k])
            lz = lz_const - self.logpi[k]
            l.append(-tf.math.add(lxz, lz))

        loss = -tf.reduce_logsumexp(l, axis=0)

        return loss

    def p_xz(self, x: TFData, k: int) -> TFData:
        var = tf.exp(self.logvar[k])

        Z = (2 * np.pi * var) ** 0.5
        return tf.math.exp(-0.5 * (x - self.mean[k]) ** 2 / var) / Z

    def p_x(self, x: TFData) -> TFData:
        pi = tf.math.softmax(self.logpi)
        px = np.zeros(len(x))

        for k in range(self.K):
            idist = pi[k]*self.p_xz(x, k)
            px = np.add(px, idist)

        return px
