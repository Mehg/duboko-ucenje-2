from __future__ import annotations

import math
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from typing import Collection, Optional

import tensorflow as tf

from tf_utils import TFData

@dataclass
class Dist(ABC):
    dim: int

    def __default_pdf(self, sample: TFData = None, raxis: Optional[int] = -1) -> TFData:
        return tf.exp(-self.score(sample, raxis=raxis))

    def __default_score(self, sample: TFData = None, raxis: Optional[int] = -1) -> TFData:
        epsilon = 1e-37
        return -tf.math.log(tf.maximum(self.pdf(sample, raxis=raxis), epsilon))

    def __post_init__(self):
        is_overriden = lambda attr: getattr(Dist, attr) != getattr(type(self), attr)

        if not (is_overriden("pdf") or is_overriden("score")):
            raise TypeError("At least one of `pdf` or `score` must be implemented.")

        if not is_overriden("pdf"):
            setattr(self, "pdf", self.__default_pdf)

        if not is_overriden("score"):
            setattr(self, "score", self.__default_score)

    @property
    @abstractmethod
    def params(self) -> Collection[str]:
        pass

    @property
    @abstractmethod
    def param_shapes(self) -> Collection[int]:
        pass

    def update_params(self, *args) -> None:
        for param, value in zip(self.params, args):
            if hasattr(self, param):
                setattr(self, param, value)
            else:
                raise ValueError(f"Parameter `{param}` of `{type(self)}` not found.")

    @abstractmethod
    def sample(self, eps: TFData = None) -> TFData:
        pass

    def score(self, sample: TFData, raxis: Optional[int] = -1) -> TFData:
        return NotImplemented

    def pdf(self, sample: TFData, raxis: Optional[int] = -1) -> TFData:
        pass

@dataclass
class Normal(Dist):
    mean: TFData = field(default=None, init=False)
    logvar: TFData = field(default=None, init=False)

    @property
    def params(self) -> Collection[str]:
        return "mean", "logvar"

    @property
    def param_shapes(self) -> Collection[int]:
        return self.dim, self.dim

    def sample(self, eps: TFData = None) -> TFData:
        if eps is None:
            eps = tf.random.normal(shape=tf.shape(self.mean))

        stddev = tf.exp(self.logvar / 2)

        return eps * stddev + self.mean

    def score(self, sample: TFData, raxis=-1) -> TFData:
        var = tf.exp(self.logvar) + 1e-7
        score = 0.5 * ((sample - self.mean) ** 2 / var + self.logvar + tf.math.log(2 * math.pi))

        if raxis is not None:
            score = tf.reduce_sum(score, axis=raxis)

        return score

@dataclass
class RectifiedNormal(Normal):
    def sample(self, eps: TFData = None) -> TFData:
        normal_sample = super().sample(eps)

        return tf.maximum(0.0, normal_sample)

    def score(self, sample: TFData, raxis: Optional[int] = -1) -> TFData:
        normal_score = super().score(sample, raxis=None)
        std = tf.exp(self.logvar / 2)
        p_0 = 0.5 + tf.math.erf(((-self.mean / std) - self.mean) / (std * tf.sqrt(2.0))) / 2 + 1e-7

        score = tf.where(sample == 0, -tf.math.log(p_0), normal_score)

        if raxis is not None:
            score = tf.reduce_sum(score, axis=raxis)

        return score

@dataclass
class Binary(Dist):
    logpi: TFData = field(default=None, init=False)

    @property
    def params(self) -> Collection[str]:
        return "logpi",

    @property
    def param_shapes(self) -> Collection[int]:
        return self.dim,

    def sample(self, eps: TFData = None) -> TFData:
        if eps is None:
            eps = tf.random.uniform(shape=tf.shape(self.logpi))

        pi = tf.nn.sigmoid(self.logpi)

        return tf.where(eps < pi, 1.0, 0.0)

    def score(self, sample: TFData, raxis: Optional[int] = -1) -> TFData:
        pscore = tf.math.softplus(-self.logpi)
        nscore = tf.math.softplus(self.logpi)
        score = sample * pscore + (1 - sample) * nscore

        if raxis is not None:
            score = tf.reduce_sum(score, axis=raxis)

        return score

@dataclass
class Beta(Dist):
    logit: TFData = field(default=None, init=False)
    logscale: TFData = field(default=None, init=False)

    @property
    def params(self) -> Collection[str]:
        return "logit", "logscale"

    @property
    def param_shapes(self) -> Collection[int]:
        return self.dim, self.dim

    def sample(self, eps: TFData = None) -> TFData:
        if eps is not None:
            raise Warning("Reparameterization trick is not implemented.")
            
        mean = tf.nn.sigmoid(self.logit)
        scale = tf.math.exp(self.logscale)

        a = mean * scale
        b = (1 - mean) * scale

        # metoda nije implementirana!
        # return tfp.distributions.Beta(a, b).quantile(eps)

        import numpy as np

        return tf.constant(np.random.beta(a, b))

    def score(self, sample: TFData, raxis: Optional[int] = -1) -> TFData:
        mean = tf.nn.sigmoid(self.logit)
        scale = tf.math.exp(self.logscale)

        a = mean * scale
        b = (1 - mean) * scale

        sample = 1e-7 + (1 - 2e-7) * sample

        score = -(a - 1) * tf.math.log(sample) - (b - 1) * tf.math.log(1 - sample)
        score += tf.math.lgamma(a) + tf.math.lgamma(b) - tf.math.lgamma(a + b)
        score = tf.reduce_sum(score, axis=1)

        return score

@dataclass
class Adapter(Dist, ABC):
    dist: Dist

    @classmethod
    def from_dist(cls, dist: Dist) -> Adapter:
        return cls(dist.dim, dist)

    @property
    def params(self) -> Collection[str]:
        return self.dist.params

    @property
    def param_shapes(self) -> Collection[int]:
        return self.dist.param_shapes

    def __getattr__(self, attr):
        return getattr(self.dist, attr)

    def __setattr__(self, attr, value):
        if attr in ["dim", "dist", "score", "pdf"]:
            super().__setattr__(attr, value)
        else:
            self.dist.__setattr__(attr, value)

    def update_params(self, *args) -> None:
        self.dist.update_params(*args)

@dataclass
class SigmoidAdapter(Adapter):
    def score(self, sample: TFData, *args, **kwargs) -> TFData:
        sample = 1e-7 + (1 - 2e-7) * sample
        logit = -tf.math.log(1.0 / sample - 1.0)
        score = self.dist.score(logit, *args, **kwargs)
        score += tf.reduce_sum(tf.math.log(sample) + tf.math.log(1.0 - sample), axis=1)

        return score

    def sample(self, eps: TFData = None) -> TFData:
        sample = self.dist.sample(eps)

        return tf.nn.sigmoid(sample)